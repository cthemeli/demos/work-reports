package gr.demokritos.workreports.data.entities;

import gr.demokritos.workreports.utils.Constants;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = Constants.ROLES)
@EntityListeners(EntityToPersistListener.class)
@DynamicUpdate
public class Role extends AbstractEntity {

    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @OneToMany(targetEntity=User.class, mappedBy="role", fetch=FetchType.EAGER)
    private List<User> users = new ArrayList<>();

    public Role() {

    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
